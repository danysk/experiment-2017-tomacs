module tomacs

import java.lang.Math.max
import java.lang.Math.min
import java.lang.Math.abs
import java.lang.Math.sin
import java.lang.Math.floor
import protelis:coord:spreading
import protelis:coord:accumulation
import protelis:state:time
import protelis:lang:utils

def minhoodloc(field, local) {
	let mf = minHood(field);
	if (mf < local) { mf } else { local }
}

// only for numeric fields
def pick(field) {
	sumHood PlusSelf(mux(nbr(self.getDeviceUID().getId()) == self.getDeviceUID().getId()) { field } else { 0 })
}

// CRF

def CRF_raise(new, old, speed, dist) {
	let dt = self.getDeltaTime();
	if (new.get(0) == 0 || new == old || minHood(nbr(old).get(0) + dist + (self.nbrLag() + dt) * old.get(1)) <= old.get(0)) {
		new
	} else {
		[old.get(0)+speed, speed/dt]
	}
}

def CRF_combine(x, dist) {
	[x.get(0)+dist, 0]
}

def CRF(source, speed, metric) {
	source = if (source) { 0 } else { Infinity };
	rep (x <- [source, 0] ) {
		CRF_raise(minhoodloc(CRF_combine(nbr(x), metric.apply()), [source, 0]), x, speed, metric.apply())
	}.get(0)
}

// CRF2


def CRF_raise2(new, old) {
	let neighborSpeed = new.get(1);
	if (neighborSpeed != 0) {
		// Growing fast
		let mySpeed = neighborSpeed * 2;
		[new.get(0) + mySpeed, mySpeed]
	} else {
		// Stable
		new.set(1, max(new.get(0) - old.get(0), 0))
	}
}

def CRF_combine2(x, dist) {
	[x.get(0) + dist, x.get(1)]
}

def CRF2(source, metric) {
	rep (x <- [Infinity, 0] ) {
		mux (source) {
			[0, 0]
		} else {
			let xf = nbr(x);
			CRF_raise2(minhoodloc(CRF_combine2(xf, metric.apply()), [Infinity, 0]), x)
		}
	}.get(0)
}

// FLEX
def FLEX_distort(mindist, metric) {
	java::lang::Math::max(metric.apply(), mindist)
}

def FLEX_raise(new, old, dist, eps, freq, rad) {
	let slopeinfo = maxHood([(old.get(0) - nbr(old.get(0))) / dist, nbr(old.get(0)), dist]);
	if (old == new || old.get(1) == freq || old.get(0) > max(2*new.get(0), rad) || new.get(0) > max(2*old.get(0), rad)) {
		new
	} else {
		if (slopeinfo.get(0) > 1+eps) {
			[slopeinfo.get(1) + (1+eps)*slopeinfo.get(2), old.get(1)+1]
		} else {
			if (slopeinfo.get(0) < 1-eps) {
				[slopeinfo.get(1) + (1-eps)*slopeinfo.get(2), old.get(1)+1]
			} else {
				[old.get(0), old.get(1)+1]
			}
		}
	}
}

def FLEX_combine(x, dist) {
	[x.get(0) + dist, 0]
}

public def FLEX(source, epsilon, frequency, distortion, radius, metric) {
	source = if (source) { 0 } else { Infinity };
	rep (x <- [source, 0] ) {
		let dist = FLEX_distort(distortion * radius, metric);
		FLEX_raise(minhoodloc(FLEX_combine(nbr(x), dist), [source, 0]), x, dist, epsilon, frequency, radius)
	}.get(0)
}



// C multipath

def extract(val, num, root) {
	[val, root.apply(val, num)]
}

def aggregate(field, local, null, potential, accumulate, root) {
	extract(
		accumulate.apply(hood((a, b) -> {accumulate.apply(a, b)}, null, field.get(1)), local),
		sumHood(mux (nbr(potential) > potential) {1} else {0}),
		root
	)
}

public def C_multipath(potential, local, null, accumulate, root) {
	rep (x <- [local, local]) {
		aggregate(mux(nbr(potential) < potential) {nbr(x)} else {[null, null]}, local, null, potential, accumulate, root);
	}.get(0)
}



// T sync

def follow(cur, lim, average, decay) {
	pick(lim) + decay.apply(average.apply(cur - lim))
}

public def T_sync(initial, value, average, decay) {
	rep (x <- initial) {
		follow(nbr(x), nbr(value), average, decay)
	}
}

public def Tfilter(signal) {
	T_sync(signal, signal, (f) -> {meanHood PlusSelf(f)}, v -> {0.2 * v})
}

// CRF3

def sum(a, b) {
	a + b
}

public def div(a, b) {
	if (b == 0) {a} else { a / b }
}

public def foreach(tuple, lambda) {
	if(tuple.isEmpty()) {
		NaN
	} else {
		lambda.apply(tuple.get(0));
		foreach(tuple.subTupleEnd(1), lambda)
	}
}

def saveG(name, value, actual) {
	env.put(name + "stab", if(env.has(name)) {abs(value - env.get(name))} else { 0 });
	env.put(name, value);
	env.put(name + "err", abs(value - actual))
}

// Move if needed
env.put("target", self.getCoordinates() + [(self.nextRandomDouble() - 0.5) / 5, (self.nextRandomDouble() - 0.5) / 5]);

// Configure round
let isSmall = env.get("small") != 0;
let isTime = env.get("time") != 0;
let curTime = self.getCurrentTime();
let sourceId = if (isSmall || isTime) { 0 } else { floor(curTime / 200) % 2 };
let isSource = self.getDeviceUID().getId() == sourceId;

// G and G'
let range = 30;
let actual = self.distanceTo(sourceId);
env.put("actualdistance", actual);
saveG("g", distanceTo(isSource), actual);
saveG("crf", CRF(isSource, range/12, () -> {self.nbrRange()}), actual);
saveG("crf2", CRF2(isSource, () -> {self.nbrRange()}), actual);
let flex = FLEX(isSource, 0.3, 10, 0.2, range, () -> {self.nbrRange()});
saveG("flex", flex, actual);

// C and C'
env.put("c", mux(isSource) { C(flex, sum, 1, 0) } else { 0 });
env.put("cmp", mux(isSource) { C_multipath(-flex, 1, 0, sum, div) } else { 0 });

// T and T'
let hperiod = 100;
let idealSin = sin(curTime * pi / hperiod);
env.put("sin", idealSin);
let idealRect = if (curTime % (hperiod * 2) > hperiod) {1} else {0};
env.put("rect", idealRect);
let noise = (2 * (self.nextRandomDouble() - 0.5)) ^ 10;
env.put("noise", noise);
let sinnoise = idealSin + noise;
env.put("sinnoise", sinnoise);
let rectnoise = idealRect + noise;
env.put("rectnoise", rectnoise);
let tsin = T(sinnoise, sinnoise, identity);
env.put("tsin", tsin);
env.put("tsinerr", abs(tsin-idealSin));
let trect = T(rectnoise, rectnoise, identity);
env.put("trect", trect);
env.put("trecterr", abs(trect-idealRect));
foreach([0.01, 0.02, 0.05, 0.1, 0.2, 0.5], alpha -> {
	let tpsin = T_sync(sinnoise, sinnoise, (f) -> {meanHood PlusSelf(f)}, v -> {alpha*v});
	let tprect = T_sync(rectnoise, rectnoise, (f) -> {meanHood PlusSelf(f)}, v -> {alpha*v});
	env.put("tpsin" + alpha, tpsin);
	env.put("tprect" + alpha, tprect);
	env.put("tpsinerr" + alpha, abs(tpsin-idealSin) ^ 2);
	env.put("tprecterr" + alpha, abs(tprect-idealRect) ^ 2);
})

