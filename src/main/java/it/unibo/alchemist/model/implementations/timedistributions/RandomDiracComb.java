package it.unibo.alchemist.model.implementations.timedistributions;

import org.apache.commons.math3.random.RandomGenerator;

import it.unibo.alchemist.model.implementations.times.DoubleTime;
import it.unibo.alchemist.model.interfaces.Time;

/**
 * A DiracComb is a sequence of events that happen every fixed time interval.
 * 
 * @param <T>
 */
public class RandomDiracComb<T> extends DiracComb<T> {

    private static final long serialVersionUID = 1L;

    public RandomDiracComb(RandomGenerator rng, Time start, double minRate, double maxRate) {
        super(start, minRate + (maxRate - minRate) * rng.nextDouble());
        if(minRate > maxRate || maxRate <= 0 || minRate <= 0) {
            throw new IllegalArgumentException("Invalid rate values: {min=" + minRate + ", max=" + maxRate + "}.");
        }
    }

    public RandomDiracComb(RandomGenerator rng, double minRate, double maxRate) {
        this(rng, new DoubleTime(minRate * rng.nextDouble()), minRate, maxRate);
        if(minRate > maxRate || maxRate <= 0) {
            throw new IllegalArgumentException("Invalid rate values: {min=" + minRate + ", max=" + maxRate + "}.");
        }
    }
    
    
}
