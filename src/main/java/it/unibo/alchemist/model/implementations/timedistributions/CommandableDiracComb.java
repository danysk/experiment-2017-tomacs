package it.unibo.alchemist.model.implementations.timedistributions;

import it.unibo.alchemist.model.implementations.times.DoubleTime;
import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Molecule;
import it.unibo.alchemist.model.interfaces.Node;
import it.unibo.alchemist.model.interfaces.Time;
import java8.util.Objects;

/**
 * A DiracComb is a sequence of events that happen every fixed time interval.
 * 
 * @param <T>
 */
public class CommandableDiracComb extends AbstractDistribution<Number> {

    private static final long serialVersionUID = -5382454244629122722L;

    private final double baseRate;
    private final Node<Number> node;
    private final Molecule molecule;

    /**
     * @param start
     *            initial time
     * @param rate
     *            how many events should happen per time unit
     */
    public CommandableDiracComb(final Time start, final double baseRate, final Node<Number> node, final Molecule readFrom) {
        super(start);
        this.node = Objects.requireNonNull(node);
        this.molecule = Objects.requireNonNull(readFrom);
        this.baseRate = baseRate;
    }

    /**
     * @param rate
     *            how many events should happen per time unit
     */
    public CommandableDiracComb(final double baseRate, final Node<Number> node, final Molecule readFrom) {
        this(new DoubleTime(), baseRate, node, readFrom);
    }

    @Override
    public double getRate() {
        final Number r = node.getConcentration(molecule);
        final double currentRate = baseRate * (r == null ? 1 : r.doubleValue());
        return currentRate;
    }
    
    protected double getCurrentInterval() {
        return 1 / getRate();
    }

    @Override
    protected void updateStatus(
            final Time curTime,
            final boolean executed,
            final double param,
            final Environment<Number> env) {
        if (executed) {
            setTau(new DoubleTime(curTime.toDouble() + getCurrentInterval()));
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[currently@" + getRate() + ']';
    }

    @Override
    public AbstractDistribution<Number> clone() {
        throw new UnsupportedOperationException();
    }

}
